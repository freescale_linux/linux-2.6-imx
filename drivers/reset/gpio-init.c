/*
 * GPIO Reset Controller driver
 *
 * Copyright 2013 Philipp Zabel, Pengutronix
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/gpio.h>
#include <linux/module.h>
#include <linux/of_gpio.h>
#include <linux/platform_device.h>
#include <linux/reset-controller.h>

static struct of_device_id gpio_init_dt_ids[] = {
	{ .compatible = "gpio-init" },
	{ }
};

MODULE_DEVICE_TABLE(of, gpio_init_dt_ids);

static int gpio_init_probe(struct platform_device *pdev)
{
	struct device_node *node = pdev->dev.of_node;
	enum of_gpio_flags flags;
	int gpio, ret,en_value;
	printk("bingking gpio_init_probe \n");
	if (!node)
		return -ENODEV;
	
	/**************************************************************/
        gpio = of_get_named_gpio_flags(node, "lcd_pwn", 0, &flags);
        en_value = (flags == OF_GPIO_ACTIVE_LOW)? 1:0;
        //gpio =  of_get_named_gpio(node, "gpios", 0);
        if(!gpio_is_valid(gpio)){
                printk("invalid lcd_pwn  gpio%d\n", gpio);
        }

        ret = devm_gpio_request(&pdev->dev, gpio, "lcd_pwn_gpio");
        if (ret) {
                printk("failed to request GPIO%d lcd_pwn_gpio\n", gpio);
                return -EINVAL;
        }
        gpio_direction_output(gpio, en_value);
        msleep(500);
	/**************************************************************/
	gpio = of_get_named_gpio_flags(node, "fec_pwn", 0, &flags);
	en_value = (flags == OF_GPIO_ACTIVE_LOW)? 1:0;
	//gpio =  of_get_named_gpio(node, "gpios", 0);
	if(!gpio_is_valid(gpio)){
		printk("invalid fec_pwn  gpio%d\n", gpio);
	}

	ret = devm_gpio_request(&pdev->dev, gpio, "fec_pwn_gpio");
	if (ret) {
		printk("failed to request GPIO%d forfec2_pwn_drv\n", gpio);
		return -EINVAL;
	}
	gpio_direction_output(gpio, en_value);
	msleep(500);

	
	/**************************************************************/
	/*gpio = of_get_named_gpio_flags(node, "fec1_pwn", 0, &flags);
	en_value = (flags == OF_GPIO_ACTIVE_LOW)? 1:0;
	//gpio =  of_get_named_gpio(node, "gpios", 0);
	if(!gpio_is_valid(gpio)){
		printk("invalid fec1_pwn  gpio%d\n", gpio);
	}

	ret = devm_gpio_request(&pdev->dev, gpio, "fec1_pwn_gpio");
	if (ret) {
		printk("failed to request GPIO%d forfec1_pwn_drv\n", gpio);
		return -EINVAL;
	}
	gpio_direction_output(gpio, en_value);
	msleep(20);*/
	/**************************************************************/
	/*gpio = of_get_named_gpio_flags(node, "fec2_pwn", 0, &flags);
	en_value = (flags == OF_GPIO_ACTIVE_LOW)? 1:0;
	//gpio =  of_get_named_gpio(node, "gpios", 0);
	if(!gpio_is_valid(gpio)){
		printk("invalid fec2_pwn  gpio%d\n", gpio);
	}

	ret = devm_gpio_request(&pdev->dev, gpio, "fec2_pwn_gpio");
	if (ret) {
		printk("failed to request GPIO%d forfec2_pwn_drv\n", gpio);
		return -EINVAL;
	}
	gpio_direction_output(gpio, en_value);
	msleep(20);*/
	/**************************************************************/
	gpio = of_get_named_gpio_flags(node, "fec1_rst", 0, &flags);
	en_value = (flags == OF_GPIO_ACTIVE_LOW)? 1:0;
	//gpio =  of_get_named_gpio(node, "gpios", 0);
	if(!gpio_is_valid(gpio)){
		printk("invalid fec1_rst  gpio%d\n", gpio);
	}
	
	ret = devm_gpio_request(&pdev->dev, gpio, "fec1_rst_gpio");
	if (ret) {
		printk("failed to request GPIO%d for ohub_rst_drv\n", gpio);
		return -EINVAL;
	}
	gpio_direction_output(gpio, 0);
	msleep(300);
	gpio_direction_output(gpio, 1);
	msleep(100);
	gpio_direction_output(gpio, 0);
	msleep(1);
	/*********************************************************/
	gpio = of_get_named_gpio_flags(node, "fec2_rst", 0, &flags);
	en_value = (flags == OF_GPIO_ACTIVE_LOW)? 1:0;
	//gpio =  of_get_named_gpio(node, "gpios", 0);
	if(!gpio_is_valid(gpio)){
		printk("invalid fec2_rst  gpio%d\n", gpio);
	}

	ret = devm_gpio_request(&pdev->dev, gpio, "fec2_rst_gpio");
	if (ret) {
		printk("failed to request GPIO%d for ohub_rst_drv\n", gpio);
		return -EINVAL;
	}
	gpio_direction_output(gpio, 0);
	msleep(300);
	gpio_direction_output(gpio, 1);
	msleep(100);
	gpio_direction_output(gpio, 0);
	msleep(1);
	
	/*********************************************************/
	gpio = of_get_named_gpio_flags(node, "ldb_blk", 0, &flags);
	en_value = (flags == OF_GPIO_ACTIVE_LOW)? 1:0;
	//gpio =  of_get_named_gpio(node, "gpios", 0);
	if(!gpio_is_valid(gpio)){
		printk("invalid ldb_blk  gpio%d\n", gpio);
	}

	ret = devm_gpio_request(&pdev->dev, gpio, "ldb_blk_gpio");
	if (ret) {
		printk("failed to request GPIO%d for ohub_rst_drv\n", gpio);
		return -EINVAL;
	}
	gpio_direction_output(gpio, 0);
	

	return 0;
}

static int gpio_init_remove(struct platform_device *pdev)
{
	return 0;
}



static struct platform_driver gpio_init_driver = {
	.probe = gpio_init_probe,
	.remove = gpio_init_remove,
	.driver = {
		.name = "gpio-init",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(gpio_init_dt_ids),
	},
};

static int __init gpio_init_init(void)
{
	printk("bingking gpio_init_init \n");
	return platform_driver_register(&gpio_init_driver);
}
arch_initcall(gpio_init_init);


static void __exit gpio_init_exit(void)
{
	platform_driver_unregister(&gpio_init_driver);
}
module_exit(gpio_init_exit);


MODULE_AUTHOR("Philipp Zabel <p.zabel@pengutronix.de>");
MODULE_DESCRIPTION("gpio init controller");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:gpio-init");
