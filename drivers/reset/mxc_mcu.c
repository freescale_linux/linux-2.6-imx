#include <linux/i2c.h>
#include <linux/string.h> 
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/random.h>
#include <linux/uaccess.h>
#include <linux/workqueue.h>
#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/bcd.h>
#define DRV_VERSION "0.0.2"
#define DEV_NAME  "mcu_i2c"
#define MAX_MCU_BUF 32

typedef enum
{
        REG_VERSION,
        REG_POWER_OFF,
}REGS;

struct delayed_work work1;
struct i2c_client* mcu_client;


static int mcu_read(struct i2c_client* client, char* dat, int len){
	struct i2c_msg msgs[2];
	char addrbuf[2]={0,0};
	char buf[MAX_MCU_BUF+2];
	int ret;

	if(len > MAX_MCU_BUF)
		len = MAX_MCU_BUF;

	msgs[0].addr = client->addr;
	msgs[0].flags = 0;
	msgs[0].len = 1;
	msgs[0].buf = addrbuf;

	msgs[1].addr = client->addr;
	msgs[1].flags = I2C_M_RD;
	msgs[1].len = len;
	msgs[1].buf = buf;

	ret = i2c_transfer(client->adapter,msgs,sizeof(msgs)/sizeof(struct i2c_msg));
	memcpy(dat, buf+2, len);
	return ret;
}

static int mcu_write(struct i2c_client* client, char *dat, int len){
	char buf[MAX_MCU_BUF+1];
	int ret;
	struct i2c_msg msgs[]={
		{client->addr, 0, len, buf},
	};
	
	if(len > MAX_MCU_BUF)
		len = MAX_MCU_BUF;
	memcpy(buf+1, dat, len);
	ret = i2c_transfer(client->adapter,msgs,sizeof(msgs)/sizeof(struct i2c_msg));
	return ret;
}

int mxc_mcu_poweroff(void)
{
	printk("mxc_mcu_poweroff ...............................  \n");
	int ret;

	char buff[MAX_MCU_BUF];
	mcu_read(mcu_client, buff, sizeof(buff));
	buff[REG_POWER_OFF] = 0xaa;
	ret = mcu_write(mcu_client, buff, sizeof(buff));
	if (ret < 0) printk("mcu i2c write error \n");
	msleep(100);
	return 0;
}

static void mcu_work1(struct work_struct *w){ 
	mxc_mcu_poweroff();
} 

static int  mxc_mcu_i2c_probe(struct i2c_client *client,
		const struct i2c_device_id *id)
{
	if (!i2c_check_functionality(client->adapter,
				I2C_FUNC_SMBUS_BYTE | I2C_FUNC_I2C))
		return -ENODEV;
	mcu_client = client;
	INIT_DELAYED_WORK(&work1, mcu_work1);
	return 0;
}

static int  mxc_mcu_i2c_remove(struct i2c_client *client)
{
	return 0;
}

static void mcu_shutdown(struct i2c_client *client){
	schedule_delayed_work(&work1, (HZ*2)); 
}

static const struct i2c_device_id mxc_mcu_i2c_id[] = {
	{ DEV_NAME, 0 },
	{},
};
MODULE_DEVICE_TABLE(i2c, mxc_mcu_i2c_id);
static struct of_device_id mxc_mcu_dt_ids[] = {
	{ .compatible = "mxc,mcu" },
	{ }
};
static struct i2c_driver mxc_mcu_i2c_driver = {
	.driver = {
		   .name = DEV_NAME,
		   },
	.probe = mxc_mcu_i2c_probe,
	.remove = mxc_mcu_i2c_remove,
	.id_table = mxc_mcu_i2c_id,
	.shutdown = mcu_shutdown,

	.driver = {
		.name	= "mcu_i2c",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(mxc_mcu_dt_ids),
	},
};

static int __init mxc_mcu_i2c_init(void)
{
	return i2c_add_driver(&mxc_mcu_i2c_driver);
}

static void __exit mxc_mcu_i2c_exit(void)
{
	i2c_del_driver(&mxc_mcu_i2c_driver);
}


module_init(mxc_mcu_i2c_init);
module_exit(mxc_mcu_i2c_exit);

MODULE_AUTHOR("wuwenbing <wuwenbing@norco.com.cn> 18038196330");
MODULE_DESCRIPTION("norco mcu dirver");
MODULE_LICENSE("GPL");
